<?php
/* Template Name: Home */
get_header();
?>
<style>
    .post-card{
        border: 1px solid #d0d0d0;
        background: #FAFAFA;
        height: 450px;
        box-sizing: border-box;
        position: relative;
        margin: 0;
        padding: 0;
        transition: all 200ms ease-out;
        -webkit-transition: all 200ms ease-out;
        cursor: pointer;
        display: inline-block;
        -webkit-transform: translateZ(0);
        overflow: hidden;
    }
    }
    .post-card:hover{
        background: #fbfbfb;
    }
    .post-card a{
        display: block;
        color: #333;
        position: absolute;
        padding: 40px;
        height: 100%;
        left: 0;
        right: 0;
    }
    .post-card header{
        position: relative;
        height: 196px;
        background: #ccc;
        background-repeat: no-repeat;
        background-position: 50% 30%;
        background-size: cover;
    }

    .post-card header h4{
        font-size: 14px;
        position: absolute;
        padding: 10px 20px;
        color: #fff;
        bottom: 0;
        left: -18px;
        background: #000;
        background: rgba(0,0,0,0.9);
        font-weight: normal;
    }

</style>

    <?php while (have_posts()) : the_post(); ?>
    <div class="col-md-4 post-card">
        <a href="people/farhad-agajan.html">
            <header style="background-image:url(https://news.microsoft.com/stories/assets/articles/farhad.jpg)">
                <h4>profile</h4>
            </header>
            <footer><h2>Farhad Agajan</h2><p>How refugees enrich their new homes with hope and grit.</p></footer>
        </a>
    </div>
    <?php endwhile; ?>
<div class="clearfix"></div>
<?php get_footer(); ?>