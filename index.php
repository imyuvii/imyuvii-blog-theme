<?php get_header(); ?>

<?php
$posts = get_posts(array(
    'posts_per_page'   => -1,
));
$featured_posts = array();
$all_posts = array();
foreach ($posts as $k => $post) {
    if($k<5) {
        array_push($featured_posts, $post);
    } else {
        array_push($all_posts, $post);
    }
}
?>
<div class="col-md-12">
    <div id="hero">
        <div class="col one">
            <div id="hero-one">
                <a href="<?php echo get_the_permalink($featured_posts[0]->ID); ?>" class="inner">
                    <figure style="background-image:url(<?php echo get_the_post_thumbnail_url($featured_posts[0]->ID); ?>)" class="grad"></figure>
                    <footer>
                        <aside style="background-color:#639322; background-color:rgba(99, 174, 34, 0.9)" class="inside">
                            <h2><?php echo get_the_category($featured_posts[0]->ID)[0]->name; ?></h2>
                        </aside>
                        <aside class="outside">
                            <div class="table">
                                <div class="table-cell">
                                    <h2><?php echo $featured_posts[0]->post_title; ?></h2>
                                    <div class="read-more">Read more <span class="arrow"></span></div>
                                </div>
                            </div>
                        </aside>
                    </footer>
                </a>
            </div>
        </div>
        <div class="col two">
            <div id="hero-two">
                <a href="<?php echo get_the_permalink($featured_posts[1]->ID); ?>" class="inner">
                    <figure style="background-image:url(<?php echo get_the_post_thumbnail_url($featured_posts[1]->ID) ?>)"></figure>
                    <footer>
                        <figcaption>
                            <h2><?php echo get_the_category($featured_posts[1]->ID)[0]->name; ?></h2>
                            <p><?php echo $featured_posts[1]->post_title; ?></p>
                        </figcaption>
                    </footer>
                </a>
            </div>
            <div id="hero-three">
                <a href="<?php echo get_the_permalink($featured_posts[2]->ID); ?>" class="inner">
                    <figure style="background-image:url(<?php echo get_the_post_thumbnail_url($featured_posts[2]->ID) ?>)"></figure>
                    <footer>
                        <figcaption>
                            <h2><?php echo get_the_category($featured_posts[2]->ID)[0]->name; ?></h2>
                            <p><?php echo $featured_posts[2]->post_title; ?></p></figcaption>
                    </footer>
                </a>
            </div>
        </div>
        <div class="col three">
            <div id="hero-four">
                <a href="<?php echo get_the_permalink($featured_posts[3]->ID); ?>" class="inner">
                    <figure style="background-image:url(<?php echo get_the_post_thumbnail_url($featured_posts[3]->ID) ?>)"></figure>
                    <footer>
                        <figcaption>
                            <h2><?php echo get_the_category($featured_posts[3]->ID)[0]->name; ?></h2>
                            <p><?php echo $featured_posts[3]->post_title; ?></p>
                        </figcaption>
                    </footer>
                </a>
            </div>
            <div id="hero-five">
                <a href="<?php echo get_the_permalink($featured_posts[4]->ID); ?>" class="inner">
                    <figure style="background-image:url(<?php echo get_the_post_thumbnail_url($featured_posts[4]->ID) ?>)"></figure>
                    <footer>
                        <figcaption>
                            <h2><?php echo get_the_category($featured_posts[4]->ID)[0]->name; ?></h2>
                            <p><?php echo $featured_posts[4]->post_title; ?></p>
                        </figcaption>
                    </footer>
                </a>
            </div>
        </div>
    </div>
</div>
<?php if (count($all_posts) > 0) : ?>
    <?php foreach ($all_posts as $post): ?>
    <div class="col-md-4 post-card">
        <a href="<?php echo get_the_permalink(); ?>">
            <header style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
                <h4><?php echo get_the_category( $post->ID )[0]->name; ?></h4>
            </header>
            <footer><h4><?php echo get_the_title(); ?></h4><p><?php echo get_the_excerpt(); ?></p></footer>
        </a>
    </div>
    <?php endforeach; ?>
    <?php // g7_pagination(); ?>
<?php else: ?>
    <?php get_template_part('content', 'none'); ?>
<?php endif; ?>
<div class="clearfix"></div>
<?php get_footer(); ?>

