<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<meta name="title" content="" />
	<meta name="keywords" content="freelancers , upwork , peopleperhour" />
	<meta name="description" content="Learn Web Design &amp; Development, Tips and Tutorials - HTML5, CSS3, JavaScript, PHP, Laravel, Yii, Yii2, Responsive Web Design." />
	<meta name="twitter:domain" content="Yuvraj Jhala , Web Developer,  Freelancer ,  Entrepreneur" />
	<?php wp_head(); ?>
    <?php if(is_home()): ?>
        <?php $container_class = ''; ?>
        <style>
            #wrapper{
                padding-top: 0px;
            }
        </style>
    <?php else: ?>
        <?php $container_class = 'container'; ?>
    <?php endif; ?>
    <?php
    $whitelist = array(
        '127.0.0.1',
        '::1'
    );
    if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)): ?>
        <script>
        <?php /** Hotjar Tracking Code for http://imyuvii.com */ ?>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:732787,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
    <?php endif; ?>
</head>
<body <?php body_class(); ?>>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-47388802-1', 'auto');
	ga('send', 'pageview');
</script>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo home_url(); ?>">Yuvraj Jhala<small>Web Guru</small></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php echo g7_menu('mainmenu'); ?>
            </div><!-- /.navbar-collapse -->
        </div>
    </div><!-- /.container-fluid -->
</nav>
	<div id="wrapper">
		<div class="<?php echo $container_class; ?>">
			<?php get_template_part('featured'); ?>
			<main>
				<?php g7_breadcrumb(); ?>